$(document).ready(function(){
  $("#daftar").click(function(){
    $("#alert").html("");
    var asal_sekolah = $('#asal-sekolah').val();
    var jenis_sma = $('#jenis-sma').find(":selected").text();
    var alamat_sekolah = $('#alamat-sekolah').val();
    var nisn = $('#nisn').val();
    var tanggal_lulus = $('#tanggal-lulus').val();
    var nilai_UAN = $('#nilai-UAN').val();
    var pilihan_1 = $('#pilihan-1').find(":selected").text();
    var pilihan_2 = $('#pilihan-2').find(":selected").text();
    var pilihan_3 = $('#pilihan-3').find(":selected").text();
    var kota_ujian = $('#kota-ujian').find(":selected").text();
    var tempat_ujian = $('#tempat-ujian').find(":selected").text();

    $.ajax({
      url: "response.php",
      type: "POST",
      data:{
        "simpan": true,
        "asal_sekolah" : asal_sekolah,
        "jenis_sma" : jenis_sma,
        "alamat_sekolah" : alamat_sekolah,
        "nisn" : nisn,
        "tanggal_lulus" : tanggal_lulus,
        "nilai_UAN" : nilai_UAN,
        "pilihan_1" : pilihan_1,
        "pilihan_2" : pilihan_2,
        "pilihan_3" : pilihan_3,
        "kota_ujian": kota_ujian,
        "tempat_ujian" : tempat_ujian
      },
      success: function(data){
          $("#pendaftaran").append(data);
      }
    });
  })
  $("#back").click(function(){
    $("#pembayaran").css("background-color", "#ecf0f1");
    $("#pembayaran > span").css("color", "#333");
    $("#pendaftaran").css("background-color", "#e74c3c");
    $("#pendaftaran > span").css("color", "white");
    $("#l-f").toggle(500);
    $("#r-f").toggle(1000);
  })
  $("#konfirmasi").click(function(){
    $("#r-f").toggle(500);
    $("#f-f").toggle(1000);
  })
  $("#done").click(function(){
    $("#pembayaran").css("background-color", "#ecf0f1");
    $("#pembayaran > span").css("color", "#333");
    $("#pendaftaran").css("background-color", "#e74c3c");
    $("#pendaftaran > span").css("color", "white");
     $("#f-f").toggle(500);
    $("#l-f").toggle(1000);
  })
  $("#r-button").click(function(){
    var username = $("#username").val();
    var password = $("#password").val();
    var name = $("#name").val();
    var email = $("#email").val();
    var repeat_password = $("#repeat-password").val();
    var no_identitas = $("#no-identitas").val();
    var birth_date = $("#birth-date").val();
    var address = $("#address").val();
    var repeat_email = $("#repeat-email").val();

    $.ajax({
      url: "response.php",
      type: "POST",
      data:{
        "register": true,
        "username": username,
        "password": password,
        "repeat_password": repeat_password,
        "name": name,
        "email": email,
        "repeat_email": repeat_email,
        "no_identitas": no_identitas,
        "birth_date": birth_date,
        "address": address
      },
      success: function(data){
        $("#register-form").append(data);
      }
    });
  })
  $("#kota-ujian").change(function() {
    var kota_ujian = $('#kota-ujian').find(":selected").text(); 

    $.ajax({
      url: "response.php",
      type: "POST",
      data:{
        "ujian": true,
        "kota_ujian": kota_ujian
      },
      success: function(data){
        $("#tempat-ujian").html("");
        $("#tempat-ujian").append(data);
      }
    });
  });
});