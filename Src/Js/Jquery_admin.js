$(document).ready(function(){
  $("#periode-pendaftaran").change(function() {
    var periode_pendaftaran = $('#periode-pendaftaran').find(":selected").text(); 

    $.ajax({
      url: "response_admin.php",
      type: "POST",
      data:{
        "periode_jenjang": true,
        "periode_pendaftaran": periode_pendaftaran
      },
      success: function(data){
        $("#jenjang-pendaftaran").html("");
        $("#jenjang-pendaftaran").append(data);
      }
    });
  });
  $("#rekap-button").click(function(){
    var periode_pendaftaran = $('#periode-pendaftaran').find(":selected").text();
    var jenjang = $('#jenjang-pendaftaran').find(":selected").text();
    $.ajax({
      url: "response_admin.php",
      type: "POST",
      data:{
        "rekap_jenjang": true,
        "periode_pendaftaran" : periode_pendaftaran,
        "jenjang": jenjang
      },
      success: function(data){
        window.location.replace("hasil_rekap.php");
      }
    });
  })

  $("#periode-diterima").change(function() {
    var periode_pendaftaran = $('#periode-diterima').find(":selected").text(); 

    $.ajax({
      url: "response_admin.php",
      type: "POST",
      data:{
        "periode_prodi": true,
        "periode_pendaftaran": periode_pendaftaran
      },
      success: function(data){
        $("#prodi-diterima").html("");
        $("#prodi-diterima").append(data);
      }
    });
  });

  $("#diterima-button").click(function(){
    var periode_pendaftaran = $('#periode-diterima').find(":selected").text();
    var prodi = $('#prodi-diterima').find(":selected").val();
    $.ajax({
      url: "response_admin.php",
      type: "POST",
      data:{
        "hasil_prodi": true,
        "periode_pendaftaran" : periode_pendaftaran,
        "prodi": prodi
      },
      success: function(data){
        window.location.replace("hasil_penerimaan.php");
      }
    });
  })
});