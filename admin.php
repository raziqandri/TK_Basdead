<?php
  session_start();
  if(!isset($_SESSION["role"])){
    header("Location: login.php");
  } else {
    if($_SESSION["role"] == "user"){
      header("Location: login.php");
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Penerimaan UI</title>
		<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    	<link rel="stylesheet" type="text/css" href="Src/Css/my-css.css">
	</head>
	<body>
		<nav class="navbar navbar-inverse" style="border-color: #00FA9A; background-color: #1a1aff;">
      		<div class="container-fluid">
        		<div class="navbar-header">
          			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>                        
          			</button>
            		<a class="navbar-brand" href="admin.php" style='color: white;'>Universitas Inovasi</a>
        		</div>
        		<div class="collapse navbar-collapse" id="myNavbar">
          			<ul class="nav navbar-nav">
                  <?php
                    if (isset($_SESSION['username'])){
                      echo '                      
                      <li>
                        <a href="rekap_pendaftaran.php" class="nav-menu" style="color: white">Rekap Pendaftaran</a>
                      </li>
                      <li>
                        <a href="pelamar_diterima.php" class="nav-menu" style="color: white">Pelamar Diterima</a>
                      </li>
                      ';
                    }
                  ?>
            			
          			</ul>
          			<ul class="nav navbar-nav navbar-right">
            			<li>
                      <?php
                        if (isset($_SESSION['username'])){
                          echo "<a style='color: white;'><span class='glyphicon glyphicon-user' style='color: white'></span> Admin </a>";
                        }
                      ?>
            			</li>
            			<li>
              				<?php
                      if (isset($_SESSION['username'])){
                          echo '<a href="logOut.php" style="color: white;">
                              <span class="glyphicon glyphicon-log-out" style="color: white;"></span> Log Out
                          </a>';
                        } else if (!isset($_SESSION['username'])) {
                        echo '<a href="login.php" style="color: white;">
                            <span class="glyphicon glyphicon-log-in" style="color: white;"></span> Log In
                          </a>';
                        }
                      ?>
            			</li>
          			</ul>
        		</div>
      		</div>
    	</nav>
	</body>
</html>