<?php
  session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Penerimaan UI</title>
		<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    	<link rel="stylesheet" type="text/css" href="Src/Css/my-css.css">
	</head>
	<body>
		<nav class="navbar navbar-inverse" style="border-color: #00FA9A; background-color: #1a1aff;">
      		<div class="container-fluid">
        		<div class="navbar-header">
          			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>                        
          			</button>
            		<a class="navbar-brand" href="index.php" style='color: white;'>Universitas Inovasi</a>
        		</div>
        		<div class="collapse navbar-collapse" id="myNavbar">
          			<ul class="nav navbar-nav">
                  <?php
                    if (isset($_SESSION['username'])){
                      echo '
                      
                      <li class="dropdown">
                        <a class="dropdown-toggle nav-menu" data-toggle="dropdown" href="#" style="color: white">Pendaftaran Semas<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="pendaftaranS1.php">Sarjana</a></li>
                          <li><a href="#">Pascasarjana</a></li>
                        </ul>
                      </li>
                      <li>
                        <a href="#" class="nav-menu" style="color: white">Riwayat Pendaftaran</a>
                      </li>
                      <li>
                        <a href="#" class="nav-menu" style="color: white">Kartu Ujian</a>
                      </li>
                      <li>
                        <a href="#" class="nav-menu" style="color: white">Hasil Seleksi</a>
                      </li>';
                    }
                    ?>
          			</ul>
          			<ul class="nav navbar-nav navbar-right">
            			<li>
                      <?php
                        if (isset($_SESSION['username'])){
                          echo "<a style='color: white;'><span class='glyphicon glyphicon-user' style='color: white'></span> ". $_SESSION['username'] . " </a>";
                        }
                      ?>
            			</li>
              				<?php
                      if (isset($_SESSION['username'])){
                          echo '<li><a href="logOut.php" style="color: white;">
                              <span class="glyphicon glyphicon-log-out" style="color: white;"></span> Log Out
                          </a></li>';
                        } else if (!isset($_SESSION['username'])) {
                        echo '<li class="dropdown">
                              <a class="dropdown-toggle nav-menu" data-toggle="dropdown" href="#" style="color: white">Log In/Register<span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="login.php">Log In</a></li>
                                <li><a href="register.php">Register</a></li>
                              </ul>
                            </li>';
                        }
                      ?>
          			</ul>
        		</div>
      		</div>
    	</nav>
      <div class="jumbotron" style="background-color: white">
        <div id = "header">
          <div class="title">
            <div class="title-word">Memberikan Inovasi</div>
            <div class="title-word">Bagi Negeri</div>
          </div>
        </div>
        </br>
      </div>
	</body>
</html>