<?php
	session_start();
	
	$user_id = "";
	if (isset($_SESSION['user_id'])) {
		$user_id = $_SESSION['user_id'];
	}

	function connectDB(){
		// Create connection
		$conn = pg_connect("host=localhost dbname=a11 user=postgres password=basdut");
			
		// Check connection
		if (!$conn) {
			die("Connection failed");
		}
		return $conn;
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Login</title>
		<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    	<script type="text/JavaScript" src="Src/Js/Jquery.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="Src/Css/my-css.css">
	</head>
  	<body>
  		<nav class="navbar navbar-inverse" style="border-color: #00FA9A; background-color: #1a1aff;">
	      	<div class="container-fluid">
	        	<div class="navbar-header">
	          		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	            		<span class="icon-bar"></span>
	            		<span class="icon-bar"></span>
	            		<span class="icon-bar"></span>                        
	          		</button>
	            	<a class="navbar-brand" href="index.php" style='color: white;'>Universitas Inovasi</a>
	        	</div>
	        <div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li>
	              		<a href="index.php" style="color: white;">
                			<span style="color: white;"></span> Contact
              			</a>
	            	</li>
	          	</ul>
	          	<ul class="nav navbar-nav navbar-right">
	          		<li><a href="register.php" style="color: white;">
						<span class="glyphicon glyphicon-log-in" style="color: white;"></span> Register
					</a></li>;
	          	</ul>
	        </div>
	    </nav>
		<!-- source : https://codepen.io/tristanS26/pen/MyYZjX -->
		<div class="box" class="lr-wrapper" align="center">
			<div class="lr-content">
                <div class="lr-head">
                  	<div class="l_b" id="login" onClick>
                    <div></div>
                    	<span>Login</span>
                  	</div>          	
                </div>
                <div class="lr-main">
					<form method="POST" id="l-f">
						<input type="text" id="l-username" name="l-username" class="form" placeholder="Username"/>
						<span id="alert-l-username"></span>
						<input type="password" id="l-password" name="l-password" class="form" placeholder="Password"/>
						<span id="alert-l-password"></span>
						</br>
						<button id="l-button" name="submit" class="submit" value="login"> Log In </button>
						<span id="alert-l-login"></span>
						<?php
							if($_SERVER["REQUEST_METHOD"] == "POST") {
								if($_POST['submit'] === 'login') {
									$conn = connectDB();
									$username = $_POST['l-username'];
									$password = $_POST['l-password'];
									if ($username == ""){
										echo "<script>$('#alert-l-username').html('Masukkan Username');</script>";
									} if ($password == ""){
										echo "<script>$('#alert-l-password').html('Masukkan Password');</script>";
									} else {
										$query = "SELECT * FROM SIRIMA.AKUN WHERE username = '$username' AND password = '$password'";
										$result = pg_query($conn, $query);
										$flag = false;
										while ($row = pg_fetch_assoc($result)) {
											echo "<script>alert('" . $row['role'] . "');</script>";
											if ($row['role'] === 't') {
												$_SESSION['role'] = "admin";
												$_SESSION['username'] = $username;
												header("location: admin.php");
												$flag = true;
											} else if ($row['role'] === 'f') {
									  			$_SESSION['role'] = "user";
									  			$_SESSION['username'] = $username;
									  			header("location: index.php");
									  			$flag = true;
									  		}
										}
									}
							   	}
							}
						?>
					</form>
                </div>
			</div>
		</div>
	</body>
</html>