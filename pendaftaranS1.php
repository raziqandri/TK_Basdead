<?php
    session_start();
    if(!isset($_SESSION["role"])){
    header("Location: login.php");
  } else {
    if($_SESSION["role"] == "admin"){
      header("Location: login.php");
    }
  }

    function connectDB(){
      // Create connection
      $conn = pg_connect("host=localhost dbname=a11 user=postgres password=basdut");
      
      // Check connection
      if (!$conn) {
        die("Connection failed");
      }
      return $conn;
    }    
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Penerimaan UI</title>
    <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="Src/Css/my-css.css">
      <script type="text/JavaScript" src="Src/Js/Jquery.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-inverse" style="border-color: #00FA9A; background-color: #1a1aff;">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
            <a class="navbar-brand" href="index.php" style='color: white;'>Universitas Inovasi</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a class="dropdown-toggle nav-menu" data-toggle="dropdown" href="#" style="color: white; background-color: black">Pendaftaran Semas<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="pendaftaranS1.php">Sarjana</a></li>
                <li><a href="#">Pascasarjana</a></li>
              </ul>
            </li>
            <li>
              <a href="#" class="nav-menu" style="color: white">Riwayat Pendaftaran</a>
            </li>
            <li>
              <a href="#" class="nav-menu" style="color: white">Kartu Ujian</a>
            </li>
            <li>
              <a href="#" class="nav-menu" style="color: white">Hasil Seleksi</a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li>
              <?php
                if (isset($_SESSION['username'])){
                  echo "<a style='color: white;'><span class='glyphicon glyphicon-user' style='color: white'></span> ". $_SESSION['username'] . " </a>";
                }
              ?>
            </li>
            <li>
              <?php
              if (isset($_SESSION['username'])){
                echo '
                <a href="logOut.php" style="color: white;">
                    <span class="glyphicon glyphicon-log-out" style="color: white;"></span> Log Out
                </a>';
              } else {
                echo '
                <a href="login.php" style="color: white;">
                    <span class="glyphicon glyphicon-log-in" style="color: white;"></span> Log In
                </a>';
              }
              ?>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div id="login-box" class="lr-wrapper" align="center">
      <div class="lr-content">
        <div class="lr-head">
          <div class="lr-l_b" id="pendaftaran">
            <div></div>
              <span>Pendaftaran</span>
          </div>
          <div class="lr-r_b" id="pembayaran">
            <div></div>
            <span>Pembayaran</span>
          </div>
        </div>
        <div class="lr-main">
          <form method="POST" id="l-f" action="javascript:login();">
            <h3>* wajib</h3>
            <input type="text" id="asal-sekolah" name="r-username" class="form" placeholder="Asal Sekolah *"/>
            <span id="alert-asal-sekolah"></span>
            <select id="jenis-sma" class="form">
              <option selected disabled>Jenis SMA *</option>
              <option value="ipa">IPA</option>
              <option value="ips">IPS</option>
              <option value="bahasa">Bahasa</option>
            </select>
            <span id="alert-jenis-sma"></span>
            <input type="text" id="alamat-sekolah" name="r-password" class="form" placeholder="Alamat Sekolah *"/>
            <span id="alert-alamat-sekolah"></span>
            <input type="text" id="nisn" name="nisn" class="form" placeholder="NISN *"/>
            <span id="alert-nisn"></span>
            </br>
            <input type="text" name="" class="form" placeholder="Tanggal Lulus" disabled style="width: 40%; font-size: 20px">
            <input type="date" id="tanggal-lulus" class="form" style="width: 54%; font-size: 20px" />
            <span id="alert-tanggal-lulus"></span>
            <input type="text" name="quantity" min="0.00" max="40.00" id="nilai-UAN" name="nilai-un" class="form" placeholder="Nilai UAN *"/>
            <span id="alert-nilai-UAN"></span>
            <select id="pilihan-1" class="form">
              <option selected disabled>Prodi Pilihan 1 *</option>
              <?php
                displayPilihan1();

                function displayPilihan1(){
                  $conn = connectDB();
                  $query = "SELECT * FROM SIRIMA.PROGRAM_STUDI WHERE jenjang = 'S1'";
                  $result = pg_query($conn, $query);
                  while ($row = pg_fetch_assoc($result)) {
                    echo '<option value="' . $row["nama"] . ' ' . $row["jenis_kelas"] . '">' . $row["nama"] . ' ' . $row["jenis_kelas"] . '</option>';
                  }
                }
              ?>
            </select>
            <span id="alert-pilihan-1"></span>
            <select id="pilihan-2" class="form">
              <option selected disabled>Prodi Pilihan 2</option>
              <?php
                displayPilihan2();

                function displayPilihan2(){
                  $conn = connectDB();
                  $query = "SELECT * FROM SIRIMA.PROGRAM_STUDI WHERE jenjang = 'S1'";
                  $result = pg_query($conn, $query);
                  while ($row = pg_fetch_assoc($result)) {
                    echo '<option value="' . $row["nama"] . ' ' . $row["jenis_kelas"] . '">' . $row["nama"] . ' ' . $row["jenis_kelas"] . '</option>';
                  }
                }
              ?>
            </select>
            <span id="alert-pilihan-2"></span>
            <select id="pilihan-3" class="form">
              <option selected disabled>Prodi Pilihan 3</option>
              <?php
                displayPilihan3();

                function displayPilihan3(){
                  $conn = connectDB();
                  $query = "SELECT * FROM SIRIMA.PROGRAM_STUDI WHERE jenjang = 'S1'";
                  $result = pg_query($conn, $query);
                  while ($row = pg_fetch_assoc($result)) {
                    echo '<option value="' . $row["nama"] . ' ' . $row["jenis_kelas"] . '">' . $row["nama"] . ' ' . $row["jenis_kelas"] . '</option>';
                  }
                }
              ?>
            </select>
            <span id="alert-pilihan-3"></span>
            <select id="kota-ujian" class="form">
              <option selected disabled>Pilihan Kota Ujian *</option>
              <?php
                displayKota();

                function displayKota(){
                  $conn = connectDB();
                  $query = "SELECT * FROM SIRIMA.LOKASI_UJIAN";
                  $result = pg_query($conn, $query);
                  while ($row = pg_fetch_assoc($result)) {
                    echo '<option value="' . $row["kota"] . '">' . $row["kota"] . '</option>';
                  }
                }
              ?>
            </select>
            <span id="alert-kota-ujian"></span>
            <select id="tempat-ujian" class="form">
              <option selected disabled>Pilihan Tempat Ujian *</option>
            </select>
            <span id="alert-tempat-ujian"></span>
            </br>
            <button id="daftar" name="submit" class="submit" value="simpan"> Simpan </button>
          </form>
          <form method="DELETE" id="r-f" action="javascript:login();">
            <h3>ID Pendaftaran : 1234</h3>
            <h3>Biaya Pendaftaran: Rp. 500.000</h3>
            <div>
              <input id="back" type="submit" name="l-submit" class="submit" value="Kembali"/>
              <input id="konfirmasi" type="submit" name="l-submit" class="submit" value="Bayar"/>
            </div>
          </form>
          <form method="DELETE" id="f-f" action="javascript:login();">
            <h3>ID Pendaftaran : 1234</h3>
            <h3>ID Pembayaran : 1541</h3>
            <h3>Nomor Kartu Ujian : 1234343565</h3>
            <input id="done" type="submit" name="l-submit" class="submit" value="Selesai"/>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>