<?php
	session_start();
	
	$user_id = "";
	if (isset($_SESSION['user_id'])) {
		$user_id = $_SESSION['user_id'];
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Register</title>
		<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    	<script type="text/JavaScript" src="Src/Js/Jquery.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="Src/Css/my-css.css">
	</head>
  	<body>
  		<nav class="navbar navbar-inverse" style="border-color: #00FA9A; background-color: #1a1aff;">
	      	<div class="container-fluid">
	        	<div class="navbar-header">
	          		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	            		<span class="icon-bar"></span>
	            		<span class="icon-bar"></span>
	            		<span class="icon-bar"></span>                        
	          		</button>
	            	<a class="navbar-brand" href="index.php" style='color: white;'>Universitas Inovasi</a>
	        	</div>
	        <div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li>
	              		<a href="index.php" style="color: white;">
                			<span style="color: white;"></span> Contact
              			</a>
	            	</li>
	          	</ul>
	          	<ul class="nav navbar-nav navbar-right">
	          		<li><a href="login.php" style="color: white;">
						<span class="glyphicon glyphicon-log-in" style="color: white;"></span> Log In
					</a></li>;
	          	</ul>
	        </div>
	    </nav>
		<!-- source : https://codepen.io/tristanS26/pen/MyYZjX -->
		<div class="box" class="lr-wrapper" align="center">
			<div class="lr-content">
                <div class="lr-head">
                  	<div class="l_b" id="login" onClick>
                    <div></div>
                    	<span>Register</span>
                  	</div>
                </div>
                <div class="lr-main">
                	<form method="POST" id="register-form">
						<input type="text" id="username" name="username" class="form" placeholder="Username"/>
						<span id="alert-username"></span>
						<input type="password" id="password" name="password" class="form" placeholder="Password"/>
						<span id="alert-password"></span>
						<input type="password" id="repeat-password" name="repeat-password" class="form" placeholder="Repeat Password"/>
						<span id="alert-repeat-password"></span>
						<input type="text" id="name" name="r-name" class="form" placeholder="Name"/>
						<span id="alert-name"></span>
						<input type="number" id="no-identitas" name="no-identitas" class="form" placeholder="Nomor Identitas"/>
						<span id="alert-no-identitas"></span>
						<select id="jenis-kelamin" class="form">
			              	<option selected disabled>Jenis Kelamin</option>
			              	<option value="laki-laki">Laki-laki</option>
			              	<option value="perempuan">Perempuan</option>
			            </select>
			            <input type="text" name="" class="form" placeholder="Tanggal Lahir" disabled style="width: 40%; font-size: 20px">
						<input type="date" id="birth-date" class="form" style="width: 54%; font-size: 20px" />
						<span id="alert-birth-date"></span>
						<input type="text" id="address" name="address" class="form" placeholder="Address"/>
						<span id="alert-address"></span>
						<input type="email" id="email" name="email" class="form" placeholder="Email"/>
						<span id="alert-email"></span>
						<input type="email" id="repeat-email" name="repeat-email" class="form" placeholder="Repeat Email"/>
						<span id="alert-no-repeat-email"></span>
						<input type="button" id="r-button" name="submit" class="submit" value="register"/>
                  	</form>
                </div>
			</div>
		</div>
	</body>
</html>