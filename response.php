<?php
	session_start();
	
	function connectDB(){
      // Create connection
      $conn = pg_connect("host=localhost dbname=a11 user=postgres password=basdut");
      
      // Check connection
      if (!$conn) {
        die("Connection failed");
      }
      return $conn;
    } 

    if (isset($_POST('register'))){
    	"username": username,
        "password": password,
        "repeat_password": repeat_password,
        "name": name,
        "email": email,
        "repeat_email": repeat_email,
        "no_identitas": no_identitas,
        "birth_date": birth_date
        if ($_POST['username'] == ""){
			echo '<script>$("#alert-username").html("Harap isi username");</script>';
			$flag = false;
		} if ($_POST['username'] != "" && !preg_match("/^[A-Za-z0-9.]$/", $_POST['username'])) {
			echo "<script> $('#alert-username').html('Username hanya dapat berisi huruf, angka dan tanda titik');</script>";
			$flag = false;
		} if ($_POST['username'] != ""){
			$conn = connectDB();
			$query = "SELECT COUNT(*) AS EXIST FROM AKUN WHERE username = $_POST['username']";
			$result = pg_query($conn, $query);
			while ($row = pg_fetch_assoc($result)) {
				if ($row["EXIST"] > 0){
					echo '<script>$("#alert-username").html("Username sudah ada");</script>';
					$flag = false;
				}
			}
		} if ($_POST['password'] == ""){
			echo '<script>$("#alert-password").html("Harap isi password");</script>';
			$flag = false;
		} if ($_POST['name'] == ""){
			echo '<script>$("#alert-name").html("Harap isi nama");</script>';
			$flag = false;
		} if ($_POST['email'] == ""){
			echo '<script>$("#alert-email").html("Harap isi email");</script>';
			$flag = false;
		} if ($_POST['repeat_email'] == ""){
			echo '<script>$("#alert-repeat-email").html("Harap masukkan email kembali");</script>';
			$flag = false;
		} if ($_POST['address'] == ""){
			echo '<script>$("#alert-address").html("Harap isi alamat");</script>';
			$flag = false;
		} if ($_POST['no_identitas'] == ""){
			echo '<script>$("#alert-no-identitas").html("Harap isi nomor identitas");</script>';
			$flag = false;
		} if ($_POST['birth_date] == ""){
			echo '<script>$("#alert-birth-date").html("Harap isi tanggal lahir");</script>';
			$flag = false;
	    } if ($_POST['password'] != "" && !preg_match("/^[A-Za-z0-9]{6,}$/", $_POST['password'])) {
			echo "<script>$('#alert-password').html('Password minimal 6 karakter');</script>";
			$flag = false;
	    } if ($_POST['no_identitas'] != "" && !preg_match("/^[0-9]{16}$/", $_POST['no_identitas'])){
			echo "<script>$('#alert-no-identitas').html('Nomor identitas hanya terdiri dari 16 angka');</script>";
			$flag = false;
	    } if ($_POST['password'] != "" && $_POST['repeat-password'] != "" && $_POST['password'] !== $_POST['repeat_password']){
			echo "<script>$('#alert-repeat-password').html('Harap asukkan password kembali');</script>";
			$flag = false;
	    } if ($_POST['email'] != "" && $_POST['repeat_email'] != "" && $_POST['email'] !== $_POST['repeat_email']){
			echo "<script>$('#alert-repeat-email').html('Harap masukkan e-mail kembali');</script>";
			$flag = false;
	    }
    }

	if (isset($_POST['ujian'])){
		$conn = connectDB();
		$kota_ujian = $_POST['kota_ujian'];
		$query = "SELECT tempat FROM SIRIMA.LOKASI_UJIAN WHERE kota = '$kota_ujian'";
		$result = pg_query($conn, $query);
		while ($row = pg_fetch_assoc($result)) {
			echo '<option value="' . $row["tempat"] . '">' . $row["tempat"] . '</option>';
		}
		exit();
	}

	if (isset($_POST['simpan'])){
		$flag = true;
		if ($_POST['asal_sekolah'] == ""){
			echo '<script>$("#alert-asal-sekolah").html("Harap isi asal sekolah");</script>';
			$flag = false;
		} if ($_POST['jenis_sma'] == "Jenis SMA *"){
			echo '<script>$("#alert-jenis-sma").html("Harap pilih jenis sma");</script>';
			$flag = false;
	    } if ($_POST['alamat_sekolah'] == "") {
			echo '<script>$("#alert-alamat-sekolah").html("Harap isi alamat sekolah");</script>';
			$flag = false;
	    } if ($_POST['nisn'] == "") {
			echo '<script>$("#alert-nisn").html("Harap isi nisn");</script>';
			$flag = false;
		} if ($_POST['tanggal_lulus'] == "") {
			echo '<script>$("#alert-tanggal-lulus").html("Harap isi tanggal lulus");</script>';
			$flag = false;
		} if ($_POST['nilai_UAN'] == ""){
			echo '<script>$("#alert-nilai-UAN").html("Harap isi nilai UAN");</script>';
			$flag = false;
		} if ($_POST['pilihan_1'] == "Prodi Pilihan 1 *"){
			echo '<script>$("#alert-pilihan-1").html("Harap isi pilihan 1");</script>';
			$flag = false;
		} if ($_POST['kota_ujian'] == "Pilihan Kota Ujian *"){
			echo '<script>$("#alert-kota-ujian").html("Harap pilih kota ujian");</script>';
			$flag = false;
		} if ($_POST['tempat_ujian'] == "Pilihan Tempat Ujian *"){
			echo '<script>$("#alert-tempat-ujian").html("Harap pilih tempat ujian");</script>';
			$flag = false;
		} if ($_POST['pilihan_1'] == $_POST['pilihan_2']){
			echo '<script>$("#alert-pilihan-2").html("Pilihan prodi 1 tidak boleh sama dengan pilihan prodi 2");</script>';
			$flag = false;
		} if ($_POST['pilihan_2'] == $_POST['pilihan_3']){
			echo '<script>$("#alert-pilihan-3").html("Pilihan prodi 2 tidak boleh sama dengan pilihan prodi 3");</script>';
			$flag = false;
		} if ($_POST['pilihan_1'] == $_POST['pilihan_3']){
			echo '<script>$("#alert-pilihan-3").html("Pilihan prodi 1 tidak boleh sama dengan pilihan prodi 3");</script>';
			$flag = false;
		} if ($_POST['nisn'] != "" && !preg_match("/^[0-9]{10}$/", $_POST['nisn'])){
			echo '<script>$("#alert-nisn").html("nisn berisi 10 angka");</script>';
			$flag = false;
		} if ($_POST['nilai_UAN'] != "" && !preg_match("/^[0-9]{1,2}.[0-9]{0,2}$/", $_POST['nilai_UAN'])){
			echo '<script>$("#alert-nilai-UAN").html("Nilai UAN tidak valid");</script>';
			$flag = false;
		}

		if ($flag === TRUE){
			$conn = connectDB();
			$query = "SELECT * FROM SIRIMA.PERIODE_PENERIMAAN WHERE status_aktif is TRUE";
			$tahun_periode = "";
			$nomor_periode = "";
			$result = pg_query($conn, $query);
			
			while ($row = pg_fetch_assoc($result)) {
				$nomor_periode = $row["nomor"];
				$tahun_periode = $row["tahun"];
			}
			/*
			$query = "INSERT INTO SIRIMA.PENDAFTARAN (id, status_lulus, status_verifikasi, npm, pelamar, nomor_periode, tahun_periode) VALUES (.., null, true, $_POST['nisn'], .., $nomor_periode, $tahun_periode )";
			$query = "INSERT INTO SIRIMA.PENDAFTARAN_SEMAS (id_pendaftaran, status_hadir, nilai_ujian, no_kartu_ujian, lokasi_kota, lokasi_tempat) VALUES (.., null, null, null, $_POST['kota_ujian'], $_POST['tempat_ujian'])";
			$query = "INSERT INTO SIRIMA.PENDAFTARAN_SEMAS_SARJANA (id_pendaftaran, asal_sekolah, jenis_sma, alamat_sekolah, nisn, tgl_lulus, nilai_uan) VALUES ( .., $_POST['asal_sekolah'], $_POST['jenis_sma'], $_POST['nisn'], .., $_POST['nilai_UAN'])";
			$query = "INSERT INTO SIRIMA.PENDAFTARAN_PRODI (id_pendaftaran, kode_prodi, status_lulus) VALUES (.., .., null)";
			*/
			$result = pg_query($conn, $query);
				echo '<script>$("#pendaftaran").css("background-color", "#ecf0f1"); $("#pendaftaran > span").css("color", "#333"); $("#pembayaran").css("background-color", "#e74c3c"); $("#pembayaran > span").css("color", "white"); $("#l-f").toggle(1000); $("#r-f").toggle(500)</script>';
		}

		exit();
	}
?>