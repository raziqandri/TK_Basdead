<?php
	session_start();
	
	function connectDB(){
      // Create connection
      $conn = pg_connect("host=localhost dbname=a11 user=postgres password=basdut");
      
      // Check connection
      if (!$conn) {
        die("Connection failed");
      }
      return $conn;
    } 

	if (isset($_POST['periode_jenjang'])){
		$conn = connectDB();
		$periode = $_POST['periode_pendaftaran'];
		list($nomor, $tahun) = split('[-]', $periode);
		$query = "SELECT DISTINCT jenjang FROM SIRIMA.JADWAL_PENTING WHERE tahun = '$tahun' AND nomor = '$nomor'";
		$result = pg_query($conn, $query);
		while ($row = pg_fetch_assoc($result)) {
			echo '<option value="' . $row["jenjang"] . '">' . $row["jenjang"] . '</option>';
		}
		exit();
	}

	if (isset($_POST['rekap_jenjang'])){
		$_SESSION['jenjang'] = $_POST['jenjang'];
		$_SESSION['periode'] = $_POST['periode_pendaftaran'];
	}

	if (isset($_POST['periode_prodi'])){
		$conn = connectDB();
		$periode = $_POST['periode_pendaftaran'];
		list($nomor, $tahun) = split('[-]', $periode);
		$query = "SELECT jenjang, nama, jenis_kelas FROM SIRIMA.PROGRAM_STUDI WHERE jenjang IN (SELECT DISTINCT jenjang FROM SIRIMA.JADWAL_PENTING WHERE tahun = '$tahun' AND nomor = '$nomor') ORDER BY jenjang ASC";
		$result = pg_query($conn, $query);
		while ($row = pg_fetch_assoc($result)) {
			 echo '<option value="' . $row["jenjang"] . "-" . $row["nama"] . '-' . $row["jenis_kelas"] . '">' . $row["jenjang"] . " " . $row["nama"] . ' ' . $row["jenis_kelas"] . '</option>';
		}
		exit();
	}

	if (isset($_POST['hasil_prodi'])){
		$prodi = $_POST['prodi'];
		list($jenjang, $program_studi, $jenis_kelas) = split('[-]', $prodi);
		$_SESSION['prodi'] = $jenjang . " " . $program_studi . " " . $jenis_kelas;
		$_SESSION['program_studi'] = $program_studi;
		$_SESSION['jenjang'] = $jenjang;
		$_SESSION['jenis_kelas'] = $jenis_kelas;
		$_SESSION['periode'] = $_POST['periode_pendaftaran'];
	}
?>